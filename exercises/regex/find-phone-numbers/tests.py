from testing import *
from testing.tests import *
from testing.assertions import *



with cumulative(), tested_function_name("find_phone_numbers"):
    find_phone_numbers = reftest()

    find_phone_numbers("+32476123456")
    find_phone_numbers("+32485789456")
    find_phone_numbers("+33485789456")
    find_phone_numbers("-32485789456")
    find_phone_numbers("   +32485789456")
    find_phone_numbers("+32485789456      ")
    find_phone_numbers("+32485789456   +32477454545   ")
    find_phone_numbers("+32485789456   +32477454545   +32477123455")
    find_phone_numbers("""+32485789456 blablabla
                          +32477454545 blablabla
                          +32477123455""")
    find_phone_numbers("""+32485789456 +451231469
                          +32477454545 +781321345
                          +32477123455""")

